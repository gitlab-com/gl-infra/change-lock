# frozen_string_literal: true

require 'spec_helper'

describe ChangeLock::Check do
  context 'when no tag arguments are specified with the default configuration' do
    subject(:check) { described_class.new }

    describe '#valid?' do
      it 'passes the check for a date outside of all windows' do
        Timecop.freeze(Time.utc(2020, 12, 4)) do
          expect(check.valid?).to be(true)
        end
        Timecop.freeze(Time.utc(2020, 11, 19)) do
          expect(check.valid?).to be(true)
        end
      end

      it 'fails the check for a date outside of all windows, with a forced failure' do
        ClimateControl.modify(CHANGE_LOCK_FORCE_FAILURE: 'true') do
          Timecop.freeze(Time.utc(2020, 12, 1)) do
            expect(check.valid?).to be(false)
          end
          Timecop.freeze(Time.utc(2020, 11, 19)) do
            expect(check.valid?).to be(false)
          end
        end
      end

      it 'fails the check early in the weekend' do
        # 2020-12-5 is a Saturday
        Timecop.freeze(Time.utc(2020, 12, 5)) do
          expect(check.valid?).to be(false)
          expect { check.valid? }.to output(/ChangeLock check failed/).to_stdout
        end
      end

      it 'fails the check on end of year holiday season' do
        time_in_change_lock = [
          Time.utc(2023, 12, 24),
          Time.utc(2023, 12, 25),
          Time.utc(2023, 12, 26),
          Time.utc(2023, 12, 28),
          Time.utc(2023, 12, 30),
          Time.utc(2024, 1, 1),
          Time.utc(2024, 1, 2)
        ]

        time_in_change_lock.each do |time|
          Timecop.freeze(time) do
            expect(check.valid?).to be(false)
          end
        end

        # expect(fake_client).to have_received(:chat_postMessage).exactly(14).times
        # expect{ check.valid? }.to output(/ChangeLock check failed/).to_stdout
      end
    end
  end

  context 'when tag is delivery with the default configuration' do
    subject(:check_delivery) { described_class.new(limit_tags: ['delivery']) }

    describe '#valid?' do
      it 'fails the check very late in the weekend window for delivery' do
        # 2020-12-7 is a Monday; 0:55 should be within the window
        Timecop.freeze(Time.utc(2020, 12, 7, 0, 55, 0)) do
          expect(check_delivery.valid?).to be(false)
          expect { check_delivery.valid? }.to output(/ChangeLock check failed/).to_stdout
        end
      end

      it 'fails the check towards the end of the weekend window for delivery' do
        # 2020-12-6 is a Sunday; 23:00 should be within the delivery window but
        # after when infra starts being allowed.
        Timecop.freeze(Time.utc(2020, 12, 6, 23, 0, 0)) do
          expect(check_delivery.valid?).to be(false)
          expect { check_delivery.valid? }.to output(/ChangeLock check failed/).to_stdout
        end
      end
    end
  end

  context 'when testing change lock override functionality' do
    subject(:check) { described_class.new }

    describe '#valid?' do
      it 'returns true when CHANGE_LOCK_OVERRIDE environment variable is set, even during a change lock period' do
        Timecop.freeze(Time.utc(2024, 12, 25)) do # Christmas Day, should be in change lock period
          ClimateControl.modify(CHANGE_LOCK_OVERRIDE: 'true') do
            expect(check.valid?).to be(true)
          end
        end
      end

      it 'returns true when changelock::override label is present in CI_MERGE_REQUEST_LABELS, even during a change lock period' do
        Timecop.freeze(Time.utc(2024, 12, 25)) do # Christmas Day, should be in change lock period
          ClimateControl.modify(CI_MERGE_REQUEST_LABELS: 'label1,changelock::override,label2') do
            expect(check.valid?).to be(true)
          end
        end
      end

      it 'returns false during a change lock period when neither CHANGE_LOCK_OVERRIDE is set nor changelock::override label is present' do
        Timecop.freeze(Time.utc(2024, 12, 25)) do # Christmas Day, should be in change lock period
          ClimateControl.modify(CI_MERGE_REQUEST_LABELS: 'label1,label2') do
            expect(check.valid?).to be(false)
          end
        end
      end
    end
  end

  context 'when tag is infra with the default configuration' do
    subject(:check_infra) { described_class.new(limit_tags: ['infra']) }

    describe '#valid?' do
      it 'passes the check early in the week for APAC infra' do
        # 2020-12-7 is a Monday, 1am should be outside the window for infra
        # but still within the window for delivery
        Timecop.freeze(Time.utc(2020, 12, 7, 1, 0, 0)) do
          expect(check_infra.valid?).to be(true)
          expect { check_infra.valid? }.not_to output.to_stdout
        end
      end
    end
  end
end
