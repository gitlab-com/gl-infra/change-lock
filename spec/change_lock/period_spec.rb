# frozen_string_literal: true

require 'spec_helper'

describe ChangeLock::Period do
  def ci_vars
    {
      CI_JOB_ID: '50',
      CI_JOB_NAME: 'gitlab-qa',
      CI_JOB_URL: 'https://example.com/gitlab-org/gitlab-foss/-/jobs/50',
      CI_PIPELINE_URL: 'https://example.com/gitlab-org/gitlab-foss/pipelines/10',
      CI_PROJECT_DIR: '/builds/gitlab-org/gitlab-foss',
      CI_PROJECT_NAME: 'gitlab-foss',
      CI_PROJECT_PATH: 'gitlab-org/gitlab-foss',
      CI_PROJECT_URL: 'https://example.com/gitlab-org/gitlab-foss'
    }
  end

  describe '#changelock?' do
    around do |ex|
      ClimateControl.modify(ci_vars) do
        Timecop.freeze(Time.utc(2020, 12, 15, 12, 0)) do
          ex.run
        end
      end
    end

    it 'reports inside the changelock window for a date' do
      expect(described_class.new('2020-12-15 00:00:00 UTC', '2020-12-16 23:59:59 UTC').changelock?).to be(true)
    end

    it 'reports inside the changelock window for a date and sends a notification' do
      period = described_class.new('2020-12-15 00:00:00 UTC', '2020-12-16 23:59:59 UTC')
      expect(period.changelock?).to be(true)
      expect { period.changelock? }.to output(/ChangeLock check failed/).to_stdout
    end

    it 'reports outside the window for a date and does not send a notification' do
      period = described_class.new('2020-12-10 00:00:00 UTC', '2020-12-11 23:59:59 UTC')
      expect(period.changelock?).to be(false)
      expect { period.changelock? }.not_to output.to_stdout
    end

    it 'raises an exception for an invalid period specifier' do
      period = described_class.new('9000', '2020-12-11 23:59:59 UTC')
      expect { period.changelock? }.to raise_error(StandardError)
    end

    it 'raises an exception if start > end' do
      period = described_class.new('2020-12-16 23:59:59 UTC', '2020-12-15 00:00:00 UTC')
      expect { period.changelock? }.to raise_error(StandardError)
    end

    it 'raises an exception if start == end' do
      period = described_class.new('2020-12-16 23:59:59 UTC', '2020-12-16 23:59:59 UTC')
      expect { period.changelock? }.to raise_error(StandardError)
    end
  end
end
