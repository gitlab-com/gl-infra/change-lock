# frozen_string_literal: true

require 'spec_helper'

describe ChangeLock::Config do
  describe '.configuration' do
    it 'returns the changelock configuration' do
      expect(described_class.configuration).to have_key('changelock_periods')
    end
  end
end
