# frozen_string_literal: true

module ChangeLock
  class CheckFailed < StandardError
  end

  class Check
    include ::SemanticLogger::Loggable

    def initialize(limit_tags: [])
      @limit_tags = limit_tags.map(&:downcase)
      @cfg = ChangeLock::Config.configuration
    end

    def valid?
      if change_lock_force?
        logger.warn('CHANGE_LOCK_FORCE_FAILURE is set, forcing failure')
        return false
      end

      check_periods do |start, finish, tags|
        next unless check_tags?(tags)

        logger.debug('Checking changelock period',
                     project_name: project_name,
                     start: start,
                     finish: finish,
                     tags: tags,
                     limit_tags: @limit_tags)

        next unless ChangeLock::Period.new(
          start, finish
        ).changelock?

        logger.info('ChangeLock check failed',
                    start: start, finish: finish)

        if change_lock_override?
          logger.warn('change lock overridden, not failing')
          next
        end

        return false
      end
      true
    end

    private

    def check_periods
      @cfg['changelock_periods'].each do |period|
        start = period['start']
        finish = period['finish']
        tags = period.fetch('tags', []).map(&:downcase)

        yield(start, finish, tags)
      end
    end

    def check_tags?(tags)
      # If no limit tags are specified as arguments
      # or if there are no tags
      # explicitly set on the period, evaluate the window
      return true if @limit_tags.empty? || tags.empty?

      # If the limit tags specified match at least one tag
      # in the config, evaluate the window
      (@limit_tags & tags).any?
    end

    def project_name
      ENV.fetch('CI_PROJECT_NAME', 'unknown project')
    end

    def change_lock_override?
      [
        ENV.key?('CHANGE_LOCK_OVERRIDE'),
        ENV['CI_MERGE_REQUEST_LABELS']
          &.split(',')
          &.include?('changelock::override')
      ].any?
    end

    def change_lock_force?
      ENV.key?('CHANGE_LOCK_FORCE_FAILURE')
    end
  end
end
