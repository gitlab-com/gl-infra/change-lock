# frozen_string_literal: true

module ChangeLock
  module Config
    CONFIG_FILE = File.expand_path('config/changelock.yml')

    def self.configuration
      @configuration ||= YAML.load_file(CONFIG_FILE)
    end
  end
end
