# frozen_string_literal: true

module ChangeLock
  class Period
    include ::SemanticLogger::Loggable

    def initialize(start, finish)
      @start = start
      @finish = finish
      @now = Time.now.utc
    end

    def changelock?
      if parse_date.all?
        check_date
      else
        check_cron
      end
    end

    private

    def check_date
      start_date, finish_date = parse_date

      if start_date >= finish_date
        raise StandardError,
              "Start date #{start_date} must be less than " \
              "#{finish_date}"
      end

      if start_date <= @now && finish_date >= @now
        changelock_notify(start_date, finish_date)
        return true
      end

      logger.debug(
        "✅ #{@now} is outside #{start_date} and #{finish_date}"
      )
      false
    end

    def check_cron
      # Checks to see if the current date
      # is between two cron specifications

      #  c  = current date
      #  s_p = start_previous
      #  f_p = finish_previous
      #  s_n = start_next
      #  f_n = finish_next
      #
      # Outside the changelock
      #
      #   su                sa
      #   22 23 24 25 26 27 28
      #      ^f_p  ^c    ^s_n
      #   29 30 01 02 03 04 05
      #      ^f_n

      # Inside the changelock
      #
      #   su                sa
      #   22 23 24 25 26 27 28
      #                  ^s_p
      #   29 30 01 02 03 04 05
      #   ^c ^f_n       ^s_n

      start_prev, start_next = parse_cron(@start)
      finish_prev, finish_next = parse_cron(@finish)

      if start_next >= finish_next
        changelock_notify(start_prev, finish_next)
        return true
      end

      logger.debug(
        "✅ #{@now} is outside the changelock window",
        previous_changelock: "#{start_prev} - #{finish_prev}",
        next_changelock: "#{start_next} - #{finish_next}"
      )
      false
    end

    def changelock_notify(start_date, finish_date)
      msg = <<~CHANGELOCK_MSG
        ⚠️ *#{project_name}* ChangeLock check failed\n
        #{@now} is between:\n*#{start_date}* and *#{finish_date}*\n

        To override the change lock period, either:
        1. Set CHANGE_LOCK_OVERRIDE=true in CI variables
        OR
        2. Add the `changelock::override` label to your merge request

        To modify change lock periods, update the configuration at:
        https://gitlab.com/gitlab-com/gl-infra/change-lock/blob/master/config/changelock.yml

      CHANGELOCK_MSG

      puts(msg)
    end

    def parse_cron(cron)
      [
        Fugit.do_parse_cron(cron).previous_time.utc,
        Fugit.do_parse_cron(cron).next_time.utc
      ]
    rescue ArgumentError
      raise StandardError,
            "Unable to parse '#{cron}', " \
            'needs to be a date or cron formatted string'
    end

    def parse_date
      @parse_date ||= [
        Fugit.parse_at(@start)&.utc,
        Fugit.parse_at(@finish)&.utc
      ]
    end

    def job_url
      ENV.fetch('CI_JOB_URL', 'http://example.com/job-url')
    end

    def pipeline_url
      ENV.fetch('CI_PIPELINE_URL', 'http://example.com/pipeline-url')
    end

    def project_name
      ENV.fetch('CI_PROJECT_NAME', 'unknown-project')
    end
  end
end
