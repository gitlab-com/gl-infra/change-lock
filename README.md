# Change Lock

## Summary

This project contains configuration and code for testing change locks in
infrastructure. There are currently two types of change locks:

1. **Infra** change locks - For all configuration changes to production infrastructure
2. **Delivery** change locks - For deployments to production stages including canary
3. **Runner** change locks - For deployment/configuratoin changes on the Linux shared runner fleet.

### Including a change lock in an existing pipeline

Add a new job

```
<name>_change_lock:
  image: registry.ops.gitlab.net/gitlab-com/gl-infra/change-lock:latest
  script:
    - bundle exec bin/change-lock --tags <name>
```

Where `<name>` is the name of the new change lock and has corresponding tags in
the config file.

### Adding a new change lock

* Create a new entry in `config/changelock.yml`: Start / Finish dates can either use cron or date syntax
* Add corresponding tags: Tag the change lock period, currently there are three
  tags `delivery`, `infra`, and `runner`.

### Overriding a change lock

If `CHANGE_LOCK_OVERRIDE` is set as a CI variable or `changelock::override` label is added to the MR the change lock will not
fail when in a change lock period.
