FROM ruby:3.3.3-alpine

COPY Gemfile .
COPY Gemfile.lock .
COPY lib /opt/change-lock/lib
COPY bin/change-lock /opt/change-lock/bin/change-lock
COPY bin/change-lock.wrapper.sh /usr/local/bin/change-lock
COPY config /opt/change-lock/config

RUN apk add --update build-base \
    && gem install bundler \
    && bundle config --local path vendor/bundle \
    && bundle install --path vendor/bundle --retry 3 --without development
